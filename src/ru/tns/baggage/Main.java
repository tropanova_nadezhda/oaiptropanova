package ru.tns.baggage;

public class Main {
    public static void main(String[] args) {

        Baggage baggage1 = new Baggage("Петров", 3, 12.3);
        Baggage baggage2 = new Baggage("Козлов", 5, 21.5);
        Baggage baggage3 = new Baggage("Уткин", 1, 8.6);
        Baggage baggage4 = new Baggage("Румянцев", 1, 4.4);
        Baggage baggage5 = new Baggage("Морозов", 4, 15.8);
        Baggage[] baggages = {baggage1, baggage2, baggage3, baggage4, baggage5};
        namelaggage(baggages);
    }

    private static void namelaggage(Baggage[] baggages) { // метод, позволяющий вывести на экран фамилии пассажиров с ручной кладью
        System.out.println("Пассажиры с ручной кладью: ");
        int baggesLenth = baggages.length;

        for (int i = 0; i < baggesLenth; i++) {
            if (baggages[i].isHandLuggage()) {
                System.out.println(baggages[i].getSurname() + " ");
            }
        }
    }
}

