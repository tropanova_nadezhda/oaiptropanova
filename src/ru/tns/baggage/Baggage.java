package ru.tns.baggage;

public class Baggage {
    private String surname; // фамилия
    private int amountOfBaggage; // количество багажных мест
    private double weight; // общий вес багажа, в килограммах

    public Baggage(String surname, int amountOfBaggage, double weight) {
        this.surname = surname;
        this.amountOfBaggage = amountOfBaggage;
        this.weight = weight;
    }

    public Baggage() {
        this("не задано", 0, 0);
    }
    public boolean isHandLuggage() { // метод, который позволяет определить относится багаж к ручной клади или нет
        boolean handLuggage = false;
        if (weight < 10 && amountOfBaggage == 1) {
            handLuggage = true;
        }
        return handLuggage;
    }

    public String getSurname() {
        return surname;
    }
}
