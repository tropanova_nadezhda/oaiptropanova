package ru.tns.temperature;

import java.util.Scanner;

/**
 * Класс для вычисления средней температуры в Октябре,
 * определения самой высокой температуры, самой низкой температуры
 * определения самого теплого дня и самого холодного дня месяца
 *
 * @author N.S.Tropanova
 */

public class Temperature {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int numberOfDays;
        System.out.print("Введите количество дней:");
        numberOfDays = scanner.nextInt();
        int temperature[] = new int[numberOfDays];
        fillTemperature(temperature);
        System.out.println("");
        System.out.println("Avg temp: " + averageTemperature(temperature));
        System.out.println("");
        int max = getMax(temperature);
        System.out.println("Maximum temperature is: " + max);
        int min = getMin(temperature);
        System.out.println("Minimum temperature is: " + min);
        сoldday(temperature, min);
        warmday(temperature, max);
    }

    /**
     * Нахождение самых холодных дней в месяце
     *
     * @param temperature массив температур
     */

    private static void сoldday(int[] temperature, int min) {
        int number[] = new int[30];
        int x = 0;
        for (int i = 0; i < temperature.length; i++) {
            if (min == temperature[i]) {
                number[x] = i + 1;
                System.out.println("Cold day: " + number[x]);
                x++;
            }
        }
    }

    /**
     * Нахождение самых теплых дней в месяце
     *
     * @param temperature массив температур
     */

    private static void warmday(int[] temperature, int max) {
        int number[] = new int[30];
        int x = 0;
        for (int i = 0; i < temperature.length; i++) {
            if (max == temperature[i]) {
                number[x] = i + 1;
                System.out.println("Warm day: " + number[x]);
                x++;
            }
        }
    }

    /**
     * Инициализация массива температур в месяце
     *
     * @param temperature массив температур
     */

    private static void fillTemperature(int temperature[]) {
        System.out.println("days temperature: ");
        for (int i = 0; i < temperature.length; i++) {
            temperature[i] = (int) (-20 + (Math.random() * 30));
            System.out.println(temperature[i]);
        }
    }

    /**
     * Вычисление минимальной температуры
     *
     * @param temperature Массив температур
     * @return Возвращает минимальное значение
     */

    private static int getMin(int temperature[]) {
        int minValue = temperature[0];
        for (int i = 1; i < temperature.length; i++) {
            if (temperature[i] < minValue) {
                minValue = temperature[i];
            }
        }
        return minValue;
    }

    /**
     * Вычисление максимальной температуры
     *
     * @param temperature Массив температур
     * @return Возвращает максимальное значение
     */

    private static int getMax(int temperature[]) {
        int maxValue = temperature[0];
        for (int i = 1; i < temperature.length; i++) {
            if (temperature[i] > maxValue) {
                maxValue = temperature[i];
            }
        }
        return maxValue;
    }

    /**
     * Вычисление средней температуры
     *
     * @param temperature массив температур
     * @return Возвращает среднее значение
     */

    private static int averageTemperature(int temperature[]) {
        int sum = 0;
        for (int temperatureOfDay : temperature) {
            sum = sum + temperatureOfDay;
        }
        return sum / temperature.length;
    }
}

