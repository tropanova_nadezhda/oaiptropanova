package ru.tns.happyticket;

/*
 * Класс для реализации действий с счастливым билетом
 *
 * @Autor Tropanova N.S.
 */

import java.util.Scanner;

public class Happyticket {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите номер вашего билета :");
        int t = scanner.nextInt();
        if (t > 999999) {
            System.out.println("Введите корректный номер!");
            return;
        }
        if (t < 1) {
            System.out.println("Введите корректный номер!");
            return;
        }
        int x6 = t / 100000;
        int x5 = t % 100000 / 10000;
        int x4 = t % 10000 / 1000;
        int x3 = t % 1000 / 100;
        int x2 = t % 100 / 10;
        int x1 = t % 10;
        int a;
        int b;
        a = x1 + x2 + x3;
        b = x4 + x5 + x6;
        if (a == b) {
            System.out.println("Билет счастливый!");
        }
        if (a > b) {
            System.out.println("Билет не счастливый!");
        }
        if (a < b) {
            System.out.println("Билет не счастливый!");
        }
    }
}
