package ru.tns.book;

public class Book {
    private String autor;
    private String name;
    private int year;

    Book(String autor, String name, int year) {
        this.autor = autor;
        this.name = name;
        this.year = year;
    }

    Book() { //  конструктор по умолчанию
        this("Не указано", "Не указано", 0);
    }


    String getName() {
        return name;
    }

    int getYear() {
        return year;
    }

    @Override
    public String toString() {
        return "(" +
                " autor - " + autor + '\'' +
                ", name  - " + name + '\'' +
                ", year -" + year +
                ')';
    }

    boolean isYears(Book book) {
        return this.year == book.getYear();
    }

}