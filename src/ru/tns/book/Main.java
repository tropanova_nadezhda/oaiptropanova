package ru.tns.book;

import java.util.ArrayList;
import java.util.Scanner;


public class Main {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        Book book1 = new Book("Достоевский", "Преступление и наказание", 2018);
        Book book2 = new Book("Тургенев", "Отцы и дети", 2016);
        Book book3 = new Book("Шолохов", "Тихий Дон", 2010);
        Book book4 = new Book("Горький", "На дне", 2017);
        Book book5 = new Book("Белинский", "О Пензе", 2015);

        ArrayList<Book> books = new ArrayList<>(); // ArrayList - Создали массив и положили в него книги которые инициализировали
        books.add(book1);
        books.add(book2);
        books.add(book3);
        books.add(book4);
        books.add(book5);
        printMessageBooks(books);
        printComparison(books);
    }

    /**
     * printMessageBooks -  метод, который сравнивает на равенство годы издания двух книг
     * printMessageBooks - выводит информацию о сравниваемых книгах
     *
     * @param books - Используем массив книг
     */

    private static void printMessageBooks(ArrayList<Book> books) {
        int booksSize = books.size();
        if (books.get(0).isYears(books.get(booksSize - 4))) {
            System.out.println("Год издания книги - " + books.get(0).getName() + "- и книги - " + books.get(booksSize - 4).getName() + " - равны ");
        } else {
            System.out.println("Год издания книги - " + books.get(0).getName() + "- и книги - " + books.get(booksSize - 4).getName() + " - не равны");
        }
    }

    /**
     * printComparison - приравнивает сравниваемые книги с 2018-м годом
     * printComparison - Выводит информацию о книге если она 2018 - го года издания
     *
     * @param books - Использую массив книг.
     */

    private static void printComparison(ArrayList<Book> books) {
        for (Book book : books) {
            if (book.getYear() == 2018) {
                System.out.println("Книга - " + book.getName() + " - 2018-го года издания \n" + book);

            }
        }
    }
}
