package ru.tns.college;
/*
 * Класс для реализации действий с колледжем
 *
 * @Autor Tropanova N.S.
 */

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Student student1 = new Student("Миронова", Gender.WOMAN, 2017, "09.02.07");
        Student student2 = new Student("Кузнецов", Gender.MAN, 2007, "09.03.20");
        Student student3 = new Student("Прокофьева", Gender.WOMAN, 2017, "09.02.07");
        Student student4 = new Student("Чистяков", Gender.MAN, 2009, "11.19.35");
        Student student5 = new Student("Тропанова", Gender.WOMAN, 2017, "09.02.07");
        Student student6 = inputStudent();
        Student[] students = {student1, student2, student3, student4, student5, student6};

        System.out.println();

        Teacher teacher1 = new Teacher("Агапов", Gender.MAN, "Физ-ра", true);
        Teacher teacher2 = new Teacher("Соломинский", Gender.MAN, "Физ-ра", true);
        Teacher teacher3 = new Teacher("Стадник", Gender.WOMAN, "английский", false);
        Teacher teacher4 = new Teacher("Лапкова", Gender.WOMAN, "Физ-ра", false);
        Teacher teacher5 = new Teacher("Воробьева", Gender.WOMAN, "матиматика", false);
        Teacher teacher6 = inputTeacher();
        Teacher[] teachers = {teacher1, teacher2, teacher3, teacher4, teacher5, teacher6};

        Person[][] people = {teachers, students};

        System.out.println();
        System.out.print("Количество девушек 2017 года поступления: " + qualityOfGirls(students));

        System.out.println();
        System.out.println("Преподаватели-кураторы: ");
        curators(teachers);
        System.out.println();
        System.out.println("Преподаватели и студенты мужского пола:");
        teachersNStudentsMale(people);
    }

    /**
     * пользователь вводит с клавиатуры студента ( его характеристики(данные) )
     *
     * @return объект студента
     */

    private static Student inputStudent() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Фамилия студента: ");
        String surname = scanner.nextLine();
        System.out.print("Пол(м или ж): ");
        String gender = scanner.nextLine();
        System.out.print("Год поступления: ");
        int entrance = scanner.nextInt();
        scanner.nextLine();
        System.out.print("Код специальности: ");
        String codeOfProfession = scanner.nextLine();
        Gender gender1 = Gender.WOMAN;
        if (gender.equalsIgnoreCase("м")) {
            gender1 = Gender.MAN;
        }
        if (gender.equalsIgnoreCase("ж")) {
            gender1 = Gender.WOMAN;
        }
        return new Student(surname, gender1, entrance, codeOfProfession);
    }

    /**
     * пользователь вводит с клавиатуры Преподавателя ( его характеристики(данные) )
     *
     * @return объект Преподавателя
     */
    private static Teacher inputTeacher() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Фамилия преподавателя: ");
        String surname = scanner.nextLine();
        System.out.print("Пол(ж или м): ");
        String gender = scanner.nextLine();
        Gender gender1 = Gender.WOMAN;
        if (gender.equalsIgnoreCase("м")) {
            gender1 = Gender.MAN;
        }
        if (gender.equalsIgnoreCase("ж")) {
            gender1 = Gender.WOMAN;
        }
        System.out.print("Дисциплина: ");
        String discipline = scanner.nextLine();
        System.out.print("Куратор(да или нет): ");
        String isCurator = scanner.nextLine();
        boolean isCurator1 = false;
        if (isCurator.equalsIgnoreCase("да")) {
            isCurator1 = true;
        }
        if (isCurator.equalsIgnoreCase("нет")) {
            isCurator1 = false;
        }
        return new Teacher(surname, gender1, discipline, isCurator1);
    }

    /**
     * Определяет количество девушек, поступивших в 2017 году.
     *
     * @param students массив студенток
     * @return количесво студенток
     */
    private static int qualityOfGirls(Student[] students) {
        int quality = 0;
        for (int i = 0; i < students.length; i++) {
            if (students[i].getEntrance() == 2017) {
                quality++;
            }
        }
        return quality;
    }

    /**
     * выводит преподавателя который является курсатором
     *
     * @param teachers массив кураторов
     */

    private static void curators(Teacher[] teachers) {
        for (int i = 0; i < teachers.length; i++) {
            if (teachers[i].isCurator()) {
                System.out.println(teachers[i] + " ");
            }
        }
    }

    /**
     * находит и выводит преподавателей и студентов мужского пола
     *
     * @param people массив преподавателей и студентов
     */
    private static void teachersNStudentsMale(Person[][] people) {
        for (int i = 0; i < people.length; i++) {
            for (int j = 0; j < people[0].length; j++) {
                if (people[i][j].getGender() == Gender.MAN) {
                    System.out.println(people[i][j] + " ");
                }
            }
        }
    }
}

