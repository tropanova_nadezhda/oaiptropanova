package ru.tns.college;

/**
 * Класс Person родительский класс у которого 2 наследника(студенты, преподаватели)
 */
class Person {
    private String surname;
    private Gender gender;

    Person(String surname, Gender gender) {
        this.surname = surname;
        this.gender = gender;
    }

    String getSurname() {
        return surname;
    }

    public Gender getGender() {
        return gender;
    }
}

