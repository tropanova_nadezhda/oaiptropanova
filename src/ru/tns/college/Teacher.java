package ru.tns.college;

/**
 * Класс Преподаватели (наследник у Person)
 */

public class Teacher extends Person {
    private String discipline;
    private boolean isCurator;

    Teacher(String surname, Gender gender, String discipline, boolean isCurator) {
        super(surname, gender);
        this.discipline = discipline;
        this.isCurator = isCurator;
    }

    @Override
    public String toString() {
        return "Преподаватели{" +
                "Фамилия=" + getSurname() +
                ", Пол=" + getGender() +
                ", Дисциплина='" + discipline + '\'' +
                ", Куратор=" + isCurator +
                '}';
    }

    public String getDiscipline() {
        return discipline;
    }

    /**
     * проверяет преподавателя является ли он курсатором
     *
     * @return выводит булевое значение (true или false)
     */

    boolean isCurator() {
        return isCurator;
    }
}

