package ru.tns.calculator;

import java.util.Scanner;

public class calcul2 {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        String ex = scanner.nextLine();
        String[] words = ex.split("\\s"); // Разбиение строки на слова с помощью разграничителя (пробел)
        findOfOpResult(words);
    }

    /**
     * Находит число до знака действия
     *
     * @param words массив с отдельными элементами строки
     * @return int первое число
     */
    private static int firstNumb(String[] words) {
        String strFirstNum = words[0];
        int intFirstNum = Integer.parseInt(strFirstNum);
        return intFirstNum;
    }

    /**
     * Находит число после знака действия
     *
     * @param words массив с отдельными элементами строки
     * @return int второе число
     */
    private static int secondNumb(String[] words) {
        String strSecondNum = words[2];
        int intSecondNum = Integer.parseInt(strSecondNum);
        return intSecondNum;
    }

    /**
     * Определяет знак операции
     * Совершает операцию над числами
     *
     * @param words результат операции
     */
    private static void findOfOpResult(String[] words) {
        int result = 0;
        int a = firstNumb(words);
        int b = secondNumb(words);

        switch (words[1]) {
            case "+":
                result = MathInt.add(a, b);
                System.out.println(a + " " + words[1] + " " + b + " = " + result);
                break;
            case "-":
                result = MathInt.sub(a, b);
                System.out.println(a + " " + words[1] + " " + b + " = " + result);
                break;
            case "*":
                result = MathInt.mult(a, b);
                System.out.println(a + " " + words[1] + " " + b + " = " + result);
                break;
            case "^":
                result = MathInt.exp(a, b);
                System.out.println(a + " " + words[1] + " " + b + " = " + result);
                break;
            case "/":
                result = MathInt.div(a, b);
                System.out.println(a + " " + words[1] + " " + b + " = " + result);
                break;
            case ":":
                result = MathInt.div(a, b);
                System.out.println(a + " " + words[1] + " " + b + " = " + result);
                break;
            case "%":
                result = MathInt.rem(a, b);
                System.out.println(a + " " + words[1] + " " + b + " = " + result);
                break;
            default:
                System.out.println("Неправильный знак операций");
                break;
        }
    }

}

