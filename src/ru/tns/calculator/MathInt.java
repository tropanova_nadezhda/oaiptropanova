package ru.tns.calculator;

public class MathInt {
    /**
     * Нахождение суммы
     *
     * @param n1 Первое слагаемое
     * @param n2 Второе слагаемое
     * @return Сумму
     */

    public static int add(int n1, int n2) {
        int result = n1 + n2;
        return result;
    }

    /**
     * Нахождение разности
     *
     * @param n1 Уменьшаемое
     * @param n2 Вычитаемое
     * @return Разность
     */

    public static int sub(int n1, int n2) {
        int result = n1 - n2;
        return result;
    }

    /**
     * Деление чисел
     *
     * @param n1 Делимое число
     * @param n2 Делитель
     * @return Частное
     */

    public static int div(int n1, int n2) {
        int result = n1 / n2;
        return result;
    }

    /**
     * Нахождение произведения
     *
     * @param n1 Первый множитель
     * @param n2 Второй множитель
     * @return Произведение
     */

    public static int mult(int n1, int n2) {
        int result = n1 * n2;
        return result;
    }

    /**
     * Возведение в степень
     *
     * @param n1 Основание степени
     * @param n2 Показатель степени
     * @return Число возведенное в степень
     */

    public static int exp(int n1, int n2) {
        int result = (int) Math.pow(n1, n2);
        return result;
    }

    /**
     * Нахождение остатка
     *
     * @param n1 Делимое число
     * @param n2 Делитель
     * @return Остаток
     */

    public static int rem(int n1, int n2) {
        int result = n1 % n2;
        return result;
    }
}

