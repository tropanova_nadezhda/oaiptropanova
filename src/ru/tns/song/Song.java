package ru.tns.song;

public class Song {
    private String name;
    private String author;
    private int time;

    public Song(String name, String author, int time) {
        this.name = name;
        this.author = author;
        this.time = time;
    }

    public Song() {
        this("Не указано", "Не указан", 0); // конструктор по умолчанию
    }

    /**
     * метод category возвращает одно из следующих значений:
     * short продолжительность менее 2-х минут
     * long продолжительность более 4-х минут
     * medium продолжительность от 2-х до 4-х минут
     */

    public String category() {
        String categoryLong = "error";
        if (time <= 120) {
            categoryLong = "short";
        }
        if (time > 120 && time <= 240) {
            categoryLong = "medium";
        }

        if (time > 240) {
            categoryLong = "long";
        }
        return categoryLong;
    }

    /**
     * Сравнивает, относится ли текущий объект и полученный,
     * к одной и той же категории
     */
    public boolean isSameCategory(Song song) {
        boolean isSameCategory = false;
        if (song.category().equals(this.category())) {
            isSameCategory = true;
        }
        return isSameCategory;
    }

    @Override
    public String toString() {
        return "Song{" +
                "name='" + name + '\'' +
                ", author='" + author + '\'' +
                ", time=" + time +
                '}';
    }

    public int getTime() {
        return time;
    }
}
