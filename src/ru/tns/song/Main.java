package ru.tns.song;

import java.util.Scanner;

public abstract class Main {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        Song song1 = new Song("Пчеловод", "Rasa", 227); // medium
        Song song2 = new Song("Зацепила", "Артур Пирожков", 170); // medium
        Song song3 = new Song("Roses", "SAINt JHN", 106); // short
        Song song4 = new Song("Dance Monkey", "Tones and I", 219); // medium
        Song song5 = new Song("Улети", "T-Fest", 227); // long

        Song[] songs = {song1, song2, song3, song4, song5};
        System.out.println("Первая и последняя песня равны? - " + song1.isSameCategory(song5));

        int lenghtSec = inputSec();
        secSong(lenghtSec, songs);

        System.out.println();

        System.out.println("Short: ");
        outPutShort(songs);

    }

    /**
     * ввод продолжительности
     *
     * @return продолжительность в секундах
     */
    private static int inputSec() {
        System.out.print("продолжительность(sec):");
        int sec = scanner.nextInt();
        return sec;
    }

    /**
     * проверяет наличие совпадений с
     * запрашиваемой продолжительностью
     *
     * @param lenghtSec продолжительность в секундах
     * @param songs     массив песен
     */
    private static void secSong(int lenghtSec, Song[] songs) {
        int songsSize = songs.length;
        boolean isHaventSong = true;
        for (int i = 0; i < songsSize; i++) {
            if (lenghtSec == songs[i].getTime()) {
                System.out.println(songs[i] + " ");
                isHaventSong = false;
            }
        }
        if (isHaventSong) {
            System.out.println("такой нет");
        }
    }

    /**
     * выводит объекты с
     * продолжительностью "short"
     *
     * @param songs массив песен
     */
    private static void outPutShort(Song[] songs) {
        int songsSize = songs.length;
        for (int i = 0; i < songsSize; i++) {
            if (songs[i].category().equals("short")) {
                System.out.println(songs[i] + " ");
            }
        }
    }
}

