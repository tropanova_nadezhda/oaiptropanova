package ru.tns.robot;

public enum Direction { // направление взгляда робота
    UP,
    DOWN,
    LEFT,
    RIGHT
}
