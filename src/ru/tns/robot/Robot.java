package ru.tns.robot;

public class Robot {
    private int x;
    private int y;
    private Direction dir;

    public Robot(int x, int y, Direction dir) {
        this.x = x;
        this.y = y;
        this.dir = dir;
    }

    public Robot(int x, int y) {
        this(x, y, Direction.UP);
    }

    public Robot() {
        this(0, 0, Direction.UP);
    }

    /**
     *  повернуться на 90 градусов по часовой стрелке
     */
    private void turnRight() {

        switch (dir) {
            case UP:
                dir = Direction.RIGHT;
                break;
            case RIGHT:
                dir = Direction.DOWN;
                break;
            case DOWN:
                dir = Direction.LEFT;
                break;
            case LEFT:
                dir = Direction.UP;
                break;
            default:
                break;
        }
    }

    /**
     * шаг в направлении взгляд и за один шаг робот изменяет одну свою координату на единицу
     */

    private void stepForward() {

        switch (dir) {
            case UP:
                y++;
                break;
            case RIGHT:
                x++;
                break;
            case DOWN:
                y--;
                break;
            case LEFT:
                x--;
                break;
            default:
                break;
        }
    }

    public void move(int endX, int endY) {
        if (endX > x) {
            while (dir != Direction.RIGHT) {
                turnRight();
            }
            while (x != endX) {
                stepForward();
            }
        }
        if (endX < x) {
            while (dir != Direction.LEFT) {
                turnRight();
            }
            while (x != endX) {
                stepForward();
            }
        }
        if (endY > y) {
            while (dir != Direction.UP) {
                turnRight();
            }
            while (y != endY) {
                stepForward();
            }
        }
        if (endY < y) {
            while (dir != Direction.DOWN) {
                turnRight();
            }
            while (y != endY) {
                stepForward();
            }
        }
    }

    public Direction getDirection() {  // текущее направление взгляда
        return dir;
    }

    public int getX() {  // текущая координата X
        return x;
    }

    public int getY() { // текущая координата Y
        return y;
    }

    @Override
    public String toString() {
        return "Robot{" +
                "x=" + x +
                ", y=" + y +
                ", dir=" + dir +
                '}';
    }
}

