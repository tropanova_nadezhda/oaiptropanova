package ru.tns.robot;
/**
 * Класс для реализации действий с роботом
 *
 * @Autor Tropanova N.S.
 */

import java.util.Scanner;

public class Main {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        Robot robot = input();
        System.out.println(robot);

        System.out.print("Введите конечную позицию x: ");
        int endX = scanner.nextInt();
        System.out.print("Введите конечную позицию y: ");
        int endY = scanner.nextInt();

        robot.move(endX, endY);
        System.out.println(robot);
    }

    private static Robot input() { // метод input() возвращает нам робота
        Direction DirectionNow = Direction.UP;
        System.out.print("Введите начальную позицию x: ");
        int x = scanner.nextInt();
        System.out.print("Введите начальную позицию y: ");
        int y = scanner.nextInt();
        System.out.print("Введите куда изначально смотрит(робот)/вправо, влево, вверх, вниз/: ");
        scanner.nextLine();
        String lookAt = scanner.nextLine();
        switch (lookAt) {
            case "вверх":
                DirectionNow = Direction.UP;
                break;
            case "вниз":
                DirectionNow = Direction.DOWN;
                break;
            case "вправо":
                DirectionNow = Direction.RIGHT;
                break;
            case "влево":
                DirectionNow = Direction.LEFT;
                break;
            default:
                break;
        }
        return new Robot(x, y, DirectionNow);
    }

}



