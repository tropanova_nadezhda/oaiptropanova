package ru.tns.rational;

public class Rational {
    private int numerator;
    private int denominator;


    public Rational(int numerator, int denominator) {
        this.numerator = numerator;
        this.denominator = denominator;
    }

    public Rational() { // конструктор по умолчанию

        this(0, 1);
    }

    /** нахождение Nod
     *
     * @param a числитель
     * @param b значинатель
     * @return nod
     */

    static int nod(int a, int b) {
        while (a != b) {
            if (a > b) {
                a = a - b;
            } else {
                b = b - a;
            }
        }
        return a;
    }

    @Override
    public String toString() {
        return numerator + "/" + denominator;
    }

// умножение

    Rational mult(Rational rational) {
        int numerator = 0;
        int denominator = 0;
        numerator = this.numerator * rational.numerator;
        denominator = this.denominator * rational.denominator;
        return new Rational(numerator, denominator).raw();
    }

// деление

    Rational div(Rational rational) {
        int numerator = 0;
        int denominator = 0;
        numerator = this.numerator * rational.denominator;
        denominator = rational.numerator * this.denominator;
        return new Rational(numerator, denominator).raw();
    }

// сложение

    Rational plus(Rational rational) {
        int numerator = 0;
        int denominator = 0;
        if (this.denominator != rational.denominator) {
            numerator = this.numerator * rational.denominator + rational.numerator * this.denominator;
            denominator = rational.denominator * this.denominator;
        }
        return new Rational(numerator, denominator).raw();
    }

// вычитание

    Rational mines(Rational rational) {
        int numerator = 0;
        int denominator = 0;
        if (this.denominator != rational.denominator) {
            numerator = this.numerator * rational.denominator - rational.numerator * this.denominator;
            denominator = rational.denominator * this.denominator;
        }
        return new Rational(numerator, denominator).raw();
    }

    /**
     * сокращение дробей
     */
    Rational raw() {
        int a = numerator;
        int b = denominator;

        a /= nod(a, b);
        b /= nod(a, b);

        return new Rational(a, b);
    }

    public int getNumerator() {
        return numerator;
    }

    public int getDenominator() {
        return denominator;
    }
}