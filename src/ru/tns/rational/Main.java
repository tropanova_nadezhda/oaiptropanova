package ru.tns.rational;

/**Класс для реализации действий с дробями
 *
 * @Autor Tropanova N.S.
 */

import java.util.Scanner;

abstract class Main {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите выражение: ");
        String expression = scanner.nextLine();
        Rational rationals[] = input(expression);
        operation(expression, rationals);
    }

    /**
     * ввод действий с дробями
     */
    private static Rational[] input(String expression) {
        String[] withoutSpace = expression.replaceAll("[\\s +:*]", "/").split("/");
        if (withoutSpace[1].equals(0) || withoutSpace[5].equals(0)) {
            System.out.println("Знаменатель не должен быть равен нулю");
        }
        int a = Integer.parseInt(withoutSpace[0]);
        int b = Integer.parseInt(withoutSpace[1]);
        Rational firstNumb = new Rational(a, b);
        int c = Integer.parseInt(withoutSpace[4]);
        int d = Integer.parseInt(withoutSpace[5]);
        Rational secondNum = new Rational(c, d);
        Rational[] rationals = {firstNumb, secondNum};
        return rationals;
    }

    /**
     * Опредеяет действие с дробями
     */
    private static void operation(String expression, Rational rationals[]) {
        String[] array = expression.split(" ");
        String exp = array[1];
        switch (exp) {
            case "+":
                Rational rational1 = rationals[0].plus(rationals[1]);
                System.out.println(rationals[0] + " " + exp + " " + rationals[1] + " = " + rational1);
                break;

            case "-":
                Rational rational2 = rationals[0].mines(rationals[1]);
                System.out.println(rationals[0] + " " + exp + " " + rationals[1] + " = " + rational2);
                break;

            case "/":
                Rational rational3 = rationals[0].div(rationals[1]);
                System.out.println(rationals[0] + " " + exp + " " + rationals[1] + " = " + rational3);
                break;

            case "*":
                Rational rational4 = rationals[0].mult(rationals[1]);
                System.out.println(rationals[0] + " " + exp + " " + rationals[1] + " = " + rational4);
                break;

            default:
                System.out.println("Знак не верный!");
        }
    }
}