package ru.tns.shape;

public class Triangle extends Shape {
    private Point a;
    private Point b;
    private Point c;

    Triangle(Color color, Point a, Point b, Point c) {
        super(color);
        this.a = a;
        this.b = b;
        this.c = c;
    }

    /**
     * нахождение площади Треугольника
     *
     * @return площадь
     */
    public double area() {
        return 0.5 * Math.abs((a.getX() - c.getX()) * (b.getY() - c.getY()) - (b.getX() - c.getX()) * (a.getY() - c.getY()));
    }

    public String toString() {
        return "Triangle{" +
                "color=" + getColor() +
                ", a=" + a +
                ", b=" + b +
                ", c=" + c +
                '}';
    }
}

