package ru.tns.shape;

public class Circle extends Shape {
    private Point center;
    private double radius;

    Circle(Color color, Point center, double radius) {
        super(color);
        this.center = center;
        this.radius = radius;
    }

    /**
     * Нахождение площади Круга
     *
     * @return площадь
     */

    public double area() {
        return Math.PI * radius * radius;
    }

    public String toString() {
        return "Circle{" +
                "color=" + getColor() +
                ", center=" + center +
                ", radius=" + radius +
                '}';
    }
}


