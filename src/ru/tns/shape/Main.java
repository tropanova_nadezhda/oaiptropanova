package ru.tns.shape;

/**
 * Класс для реализации действий с фигурами
 *
 * @Autor Tropanova N.S.
 */
public class Main {
    public static void main(String[] args) {
        Square square = new Square(Color.YELLOW, new Point(3, 5), 10);
        Circle circle = new Circle(Color.RED, new Point(1, 3), 5);
        Triangle triangle = new Triangle(Color.GREEN, new Point(1, 2), new Point(1, 1), new Point(2, 1));

        Shape[] shapes = {square, circle, triangle};

        printArrayElements(shapes);

        System.out.println("Фигура с максимальным квадратом: " + shapes[maxShapeArea(shapes)]);
    }

    private static void printArrayElements(Shape[] shapes) {
        for (int index = 0; index < shapes.length; index++) {
            System.out.println(shapes[index]);
        }
    }

    private static int maxShapeArea(Shape[] shapes) {
        int maxSquare = 0;

        for (int index = 0; index < shapes.length; index++) {
            if (shapes[index].area() > shapes[maxSquare].area()) {
                maxSquare = index;
            }
        }

        return maxSquare;
    }
}
