package ru.tns.shape;

public class Square extends Shape {
    private Point corner;
    private double side;

    Square(Color color, Point corner, double side) {
        super(color);
        this.corner = corner;
        this.side = side;
    }

    /**
     * Нахождение площади Квадрата
     *
     * @return площадь
     */
    public double area() {
        return side * side;
    }

    public String toString() {
        return "Circle{" +
                "color=" + getColor() +
                ", corner=" + corner +
                ", side=" + side +
                '}';
    }
}