package ru.tns.country;
/**
 * Класс для реализации действий со странами
 *
 * @Autor Tropanova N.S.
 */

import java.util.ArrayList;
import java.util.Scanner;


public class Demo {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        Country russia = new Country("Россия", "Москва", 872483663, 4556776);

        System.out.print("Сколько стран вы хотите ввести: ");
        int num = scanner.nextInt();
        ArrayList<Country> countries = new ArrayList<>();

        for (int i = 0; i < num; i++) {
            countries.add(input());
        }

        countries.add(russia);
        System.out.println("Самая населенная страна: " + countries.get(witchTheBiggestCountry(countries)));


    }

    /**
     * пользователь вводит с клавиатуры Страну с её характеристиками
     *
     * @return объект Страна
     */
    private static Country input() {
        System.out.print("Введите название:");
        scanner.nextLine();
        String name = scanner.nextLine();
        System.out.print("Введите столицу:");
        String capital = scanner.nextLine();
        System.out.print("Введите площадь:");
        double place = scanner.nextDouble();
        System.out.print("Введите население (целое число):");
        int people = scanner.nextInt();
        return new Country(name, capital, place, people);
    }

    /**
     * Выведит характеристики страны с наибольшей плотностью населения.
     *
     * @param countries страны
     * @return страна с наибольшей плотностью населения
     */
    private static int witchTheBiggestCountry(ArrayList<Country> countries) {
        int countriesSize = countries.size();
        double max = 0;
        int part = 0;

        for (int i = 0; i < countriesSize; i++) {
            double density = countries.get(i).density();
            if (max < density) {
                part = i;
                max = density;
            }
        }
        return part;
    }
}