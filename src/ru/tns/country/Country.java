package ru.tns.country;

public class Country {
    private String name;
    private String capital;
    private double place;
    private int people;

    public Country(String name, String capital, double place, int people) {
        this.name = name;
        this.capital = capital;
        this.place = place;
        this.people = people;
    }

    public Country() {
        this("Неизвестно", "Неизвестно", 0, 0);
    }

    public double density() {
        return people / place;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public double getPlace() {
        return place;
    }

    public void setPlace(double place) {
        this.place = place;
    }

    public int getPeople() {
        return people;
    }

    public void setPeople(int people) {
        this.people = people;
    }

    @Override
    public String toString() {
        return "Country{" +
                "name='" + name + '\'' +
                ", capital='" + capital + '\'' +
                ", place=" + place +
                ", people=" + people +
                '}';
    }
}
