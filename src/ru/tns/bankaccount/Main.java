package ru.tns.bankaccount;
/* Класс Main, для реализации действий с банком
 */
public class Main {
    public static void main(String[] args) {
        Bank bank = new Bank(834.5, 10.0);
        System.out.println(bank);
        System.out.printf("Состояние банковского счёта с учетом процентных начислений за один год: %.2f", bank.podschet());

    }

}

