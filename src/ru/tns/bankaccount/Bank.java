package ru.tns.bankaccount;

public class Bank {
    double balance; // баланс, в рублях
    double interestRate; // процентная ставка, в процентах

    public Bank(double balance, double interestRate) {
        this.balance = balance;
        this.interestRate = interestRate;
    }

    /**
     * метод podschet() считает начисление процентов за год
     *
     * @return состояние банковского счета с учетом процентных начислений
     */

    public double podschet() {
        double balance1 = balance / 100;
        double balanceWithInterestRate = balance + (balance1 * interestRate);
        return balanceWithInterestRate;
    }

    @Override
    public String toString() {
        return "Bank{" +
                " Баланс =" + balance +
                ", Процентная ставка =" + interestRate +
                '}';
    }

}




