package ru.tns.game;

import java.util.Scanner;

/*
 * Класс для реализации действий с числами
 *
 * @Autor Tropanova N.S.
 */

public class Game {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int guess;
        int start = 0;
        int fin = 100;
        System.out.println("угадайте случайное число от 1 до 100: ");

        int num = start + (int) (Math.random() * (fin - start + 1));
        System.out.println(num);

        for (int i = start; i <= fin; i++) {

            guess = scanner.nextInt();
            if (num == guess) {
                System.out.println("правильно");
                return;
            }

            if (num < guess) {
                System.out.println("заданное число меньше");
            }

            if (num > guess) {
                System.out.println("заданное число больше");
            }

            if (guess < 0) {
                System.out.println("число должно быть больше 0");
            }
        }
    }
}