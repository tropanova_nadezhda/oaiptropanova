package ru.tns.patient;

public class Patient {
    private String surname; // фамилия
    private int birthYear; // год рождения
    private int cardNumber; // номер карточки
    private boolean medicalExam; // диспансеризация



    public Patient(String surname, int birthYear, int cardNumber, boolean medicalExam) { // конструктор с параметрами, позволяющий инициализировать объект при объявлении.
        this.surname = surname;
        this.birthYear = birthYear;
        this.cardNumber = cardNumber;
        this.medicalExam = medicalExam;
    }

    public Patient() {
        this("Не указано", 0, 0, false); // конструктор по умолчанию
    }

    public String getSurname() {
        return surname;
    }

    public int getBirthYear() {
        return birthYear;
    }

    public int getCardNumber() {
        return cardNumber;
    }

    public boolean isMedicalExam() {
        return medicalExam;
    }

    public String toString() { // метод toString(), позволяющий сформировать строку с характеристиками пациента
        return "Patient{" +
                "surname: " + surname +
                "; year of birth: " + birthYear +
                "; card's number: " + cardNumber +
                "; medical examination: " + ((medicalExam == true)?"completed":"not completed") +
                '}';
    }

}
