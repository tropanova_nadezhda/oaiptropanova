package ru.tns.patient;

    public  class Demo {
        public static void main(String[] args) {
            Patient[] patients = new Patient[5];

            patients[0] = new Patient("Рожков", 1941, 1, true);
            patients[1] = new Patient("Панин", 1979, 2, true);
            patients[2] = new Patient("Кошкин", 1955, 3, false);
            patients[3] = new Patient("Соколов", 2001, 4, true);
            patients[4] = new Patient("Власов", 2000, 5, true);

            showCompleted(patients);
        }

        private static void showCompleted(Patient[] patients) {
            for(int index = 0; index < patients.length; index++) {
                if(patients[index].isMedicalExam() && patients[index].getBirthYear() < 2000) {
                    System.out.println(patients[index]);
                }
            }
        }
    }

