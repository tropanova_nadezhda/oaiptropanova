package ru.tns.seabattle;


import java.util.ArrayList;

class Ship {
    private ArrayList<Integer> location = new ArrayList<>();

    Ship(int shipLength, int startCoordinate) {
        for (int i = startCoordinate; i < startCoordinate + shipLength; i++) {
            location.add(i);
        }
    }

    /**
     * определяет попал, потопил или промахнулся пользователь в корабль
     *
     * @param shot ячейка выстрела
     * @return состояние корабля
     */
    String shot(int shot) {
        String message = "Мимо";
        if (location.contains(shot)) {
            location.remove(location.indexOf(shot));
            if (location.isEmpty()) {
                message = "Потоплен";
            }
            message = "Попал";
        }
        return message;
    }

    public String toString() {
        return "Ship{" +
                "ship=" + location +
                '}';
    }
}

