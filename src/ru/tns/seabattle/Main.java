package ru.tns.seabattle;
/**
 * Класс для реализации действий с кораблем
 *
 * @Autor Tropanova N.S.
 */

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int length = (int) (Math.random() * 4);
        int gateway = (length + 1) + (int) (Math.random() * (20 - length));
        int randomStartCoordinate = (int) (Math.random() * (gateway - length));
        System.out.println("Длина корабля:" + length);
        System.out.println("Длина шлюза:" + gateway);


        Ship ship = new Ship(randomStartCoordinate, length);
        System.out.println(ship);
    }

    /**
     * выполняет выстрел по кораблю
     *
     * @param gateway шлюз
     * @param ship    длина корабля
     */
    private static void startGame(int gateway, Ship ship) {
        Scanner scanner = new Scanner(System.in);
        int shot;
        int attemptShot = 0;
        String result;
        do {
            System.out.println("Введите ячейку куда будем стрелять: ");
            shot = scanner.nextInt();
            result = ship.shot(shot);
            System.out.println(result);
            attemptShot++;
        } while (!result.equals("Потоплен"));
        System.out.println("Корабль был потоплен с " + attemptShot + " попытки");
    }
}

