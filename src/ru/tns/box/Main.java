package ru.tns.box;
import java.util.Arrays;
public class Main {

        public static void main(String[] args) {
            Box launchBox = new Box("ланчбокс", 10, 6, 12);
            Box safe = new Box("сейф", 200, 100, 150);
            Box standardBox = new Box();
            Box cubeBox = new Box("кубик", 5);
            System.out.println("Ланч-бокс: " + launchBox
                    + ", объем = " + launchBox.volume());
            System.out.println("Сейф: " + safe
                    + ", объем = " + safe.volume());
            System.out.println("Стандартная: " + standardBox
                    + ", объем = " + standardBox.volume());
            System.out.println("Кубик: " + cubeBox
                    + ", объем = " + cubeBox.volume());


            Box[] cubes = {launchBox, safe, standardBox, cubeBox};
            witchTheBiggestBox(cubes);

        }

        private static void witchTheBiggestBox(Box[] cubes) {
            int max = 0;
            int part=0;

            for (int i = 0; i < cubes.length; i++) {
                int vol = cubes[i].volume();;
                if (max<vol) {
                    part = i;
                    max=vol;
                }
            }
            System.out.println("Самая большая коробка: "+cubes[part].getName());
        }

        private static void outArrayOfBox(Box[] cubes) {
            for (int i = 0; i < cubes.length; i++) {
                System.out.println((i + 1) + " коробочка " + cubes[i]);

            }
        }
    }

