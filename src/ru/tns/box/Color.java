package ru.tns.box;

public enum Color {
    WHITE,
    YELLOW,
    RED,
    BLUE,
    BLACK,
    GREEN
}
