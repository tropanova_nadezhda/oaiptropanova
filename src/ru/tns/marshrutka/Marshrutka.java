package ru.tns.marshrutka;

import java.util.Scanner;

/**
 * Класс для реализации действий с маршруткой
 *
 * @Autor Tropanova N.S.
 */

public class Marshrutka {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int site;
        int x = 1;
        int cost = 0;

        System.out.println("введите цену билета: ");
        int price = scanner.nextInt();
        if (price > 30) {
            System.out.println("задайте другую цену! ");
            return;
        }
        if (price < 0) {
            System.out.println("задайте другую цену! ");
            return;
        }

        System.out.println("введите число пассажиров: ");
        site = scanner.nextInt();
        if (site < 0 && site > 50) {
            System.out.println("число пассажиров некоректно!");

            return;
        }

        do {
            cost = cost + price;
            x++;
            System.out.println(x + "человек ");
            System.out.println(cost + "руб");

        } while (x <= site);
    }
}