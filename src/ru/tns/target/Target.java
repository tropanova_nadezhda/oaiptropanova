package ru.tns.target;
/*
 * Класс для реализации действий с мишенью
 *
 * @Autor Tropanova N.S.
 */

import java.util.Scanner;

public class Target {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        double x;
        double y;
        System.out.print("Задайте значение x -");
        x = scanner.nextDouble();
        System.out.print("Задайте значение y -");
        y = scanner.nextDouble();
        double n = x * x + y * y;

        if (n <= 1) {
            System.out.print("набрано 10 баллов");
            return;
        }
        if (n > 1 && n <= 4) {
            System.out.print("Набрано 8 баллов");
            return;
        }
        if (n > 4 && n <= 9) {
            System.out.print("Набрано 6 баллов");
            return;
        }
        if (n > 9 && n <= 16) {
            System.out.print("Набрано 4 балла");
            return;
        }
        if (n > 16 && n <= 25) {
            System.out.print("Набрано 2 балла");
            return;
        }
        if (n > 25 && n <= 36) {
            System.out.print("Набрано 1 балл");
            return;
        }
        if (n > 36) {
            System.out.print("Набрано 0 баллов");
        }
    }
}